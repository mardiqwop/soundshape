# Soundshape
## Gregory Hughes, Daniel Reiter, & Mardigon Toler

An audio plugin for synthesizing audio from a spectrum.
 
 
[Python FFT Synthesis Demo](https://nbviewer.jupyter.org/urls/gitlab.com/mardiqwop/soundshape/raw/master/notebooks/FFT_Synthesis_Example.ipynb)
